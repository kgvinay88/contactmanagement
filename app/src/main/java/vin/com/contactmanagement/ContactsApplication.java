package vin.com.contactmanagement;

import android.app.Application;

import vin.com.contactmanagement.di.components.ApplicationComponent;
import vin.com.contactmanagement.di.components.DaggerApplicationComponent;
import vin.com.contactmanagement.di.modules.ApplicationModule;

/**
 * Created by Vinay .
 */

public class ContactsApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        initializeInjector();
        super.onCreate();
    }


    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }


    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
