package vin.com.contactmanagement.domain;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay
 */

public class GetDetailsUseCase extends Usecase<Details> {


    private final ContactsRepository mRepository;
    private final Scheduler mUiThread;
    private final Scheduler mExecutorThread;
    private String mContactID;

    @Inject
    public GetDetailsUseCase(
            ContactsRepository repository,
            @Named("ui_thread") Scheduler uiThread,
            @Named("executor_thread") Scheduler executorThread) {

        mRepository = repository;
        mUiThread = uiThread;
        mExecutorThread = executorThread;
    }

    public void setContactID(String contactID){
        mContactID = contactID ;
    }


    @Override
    public Observable<Details> buildObservable() {
        return mRepository.getDetails(mContactID)
                .observeOn(mUiThread)
                .subscribeOn(mExecutorThread);
    }
}
