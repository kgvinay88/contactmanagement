package vin.com.contactmanagement.domain;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay
 */

public class UpdateContactUseCase extends Usecase<Details> {

    private final ContactsRepository mRepository;
    private final Scheduler mUiThread;
    private final Scheduler mExecutorThread;
    private Details mContactDetails;
    private String mContactID;

    @Inject
    public UpdateContactUseCase(
            ContactsRepository repository,
            @Named("ui_thread") Scheduler uiThread,
            @Named("executor_thread") Scheduler executorThread) {

        mRepository = repository;
        mUiThread = uiThread;
        mExecutorThread = executorThread;
    }

    public void setContactDetails(String contactID,Details contactDetails){
        mContactID = contactID ;
        mContactDetails = contactDetails ;
    }

    @Override
    public Observable<Details> buildObservable() {
        return mRepository.updateContact(mContactID,mContactDetails)
                .observeOn(mUiThread)
                .subscribeOn(mExecutorThread);
    }
}
