package vin.com.contactmanagement.domain;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay
 */

public class GetContactsDataUseCase extends Usecase<List<Contacts>> {


    private ContactObservableRepoDb mDatabase;
    private final ContactsRepository mRepository;
    private final Scheduler mUiThread;
    private final Scheduler mExecutorThread;

    @Inject
    public GetContactsDataUseCase(
            ContactsRepository repository,
            @Named("ui_thread") Scheduler uiThread,
            @Named("executor_thread") Scheduler executorThread,ContactObservableRepoDb database) {

        mRepository = repository;
        mUiThread = uiThread;
        mExecutorThread = executorThread;
        mDatabase = database;
    }

    @Override
    public Observable<List<Contacts>> buildObservable() {
        return  mDatabase.getObservable()
                .observeOn(mUiThread)
                .subscribeOn(mExecutorThread);
    }

    public Observable<List<Contacts>> updateRepo() {
        BehaviorSubject<List<Contacts>> requestSubject = BehaviorSubject.create();

        Observable<List<Contacts>> observable = mRepository.getContacts();
        observable.observeOn(mUiThread)
                .subscribeOn(mExecutorThread)
                .subscribe(contactsList -> {mDatabase.insertRepoList(contactsList);
                                            requestSubject.onNext(contactsList);},
                        requestSubject::onError,
                        requestSubject::onCompleted);
        return requestSubject.asObservable();
    }
}
