package vin.com.contactmanagement.mvp.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.inject.Inject;

import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.domain.GetContactsDataUseCase;
import vin.com.contactmanagement.mvp.HomeView;
import vin.com.contactmanagement.mvp.View;

/**
 * Created by Vinay
 */

public class HomePresenter implements Presenter {

    private HomeView mView;
    private final GetContactsDataUseCase mContactsUseCase;


    @Inject
    public HomePresenter(GetContactsDataUseCase starredDataUseCase) {
        mContactsUseCase = starredDataUseCase;
    }

    @Override
    public void attachView(View v) {
        mView = (HomeView) v;
    }

    public void initialize() {
        mView.displayLoadingScreen();
        mContactsUseCase.execute()
                .subscribe(this::onContactsReceived, this::onContactsError);
        mContactsUseCase.updateRepo();
    }

    public void onContactsError(Throwable throwable) {
        mView.hideLoadingScreen();
        mView.displayErrorMessage("Unable to load data");
        throwable.printStackTrace();
    }

    public void onContactsReceived(List<Contacts> contacts) {
        mView.hideLoadingScreen();
        Collections.sort(contacts, new AlphabeticalComparator());
        mView.updateContactsList(contacts);
    }

    private class AlphabeticalComparator implements Comparator<Contacts> {
        @Override
        public int compare(Contacts o1, Contacts o2) {
            return o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
        }
    }
}
