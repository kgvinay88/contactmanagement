package vin.com.contactmanagement.mvp;

import vin.com.contactmanagement.data.entity.Details;

/**
 * Created by Vinay
 */

public interface DetailsView extends View{

    void displayLoadingScreen();

    void hideLoadingScreen();

    void updateContactDetails(Details contactDetails);

    void setFavouriteContact();

    void displayErrorMessage(String errorMessage);
}
