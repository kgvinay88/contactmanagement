package vin.com.contactmanagement.mvp.view.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vin.com.contactmanagement.R;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.mvp.view.AddView;

public class AddContactActivity extends BaseActivity implements AddView {

    private static final int CAMERA_PERMISSION_CODE = 111;
    private static final int CAMERA_INTENT = 1;

    @BindView(R.id.addContactsCoordinatorLayout)
    CoordinatorLayout mAddContactsCoordinatorLayout;

    @BindView(R.id.addContactsToolbar)
    Toolbar mToolBar;

    @BindView(R.id.iv_add_profile_icon)
    de.hdodenhof.circleimageview.CircleImageView mProfileIcon;

    @BindView(R.id.et_add_first_name)
    TextView mFirstNameTextView;

    @BindView(R.id.et_add_last_name)
    TextView mLastNameTextView;

    @BindView(R.id.et_add_email)
    TextView mEmailTextView;

    @BindView(R.id.et_add_mobile)
    TextView mPhoneTextView;

    ProgressDialog mProgressDialog;
    private Uri file;
    private String mFName;
    private String mLName;
    private String mEmail;
    private String mPhone;
    private String mProfilePICURL;
    private boolean mFavorite;
    private String mCreatedAt;
    private String mUpdatedAt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        ButterKnife.bind(this);
        setupToolBar();
        setupProgressDialog();
        mAddContactPresenter.attachView(this);

    }

    private void setupToolBar() {
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.iv_add_profile_icon)
    public void addProfileIcon() {
        displayProfileIconPickerDialog();
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading . . . ");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }

    @Override
    public void displayLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.show();
    }

    @Override
    public void hideLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public void updateContactAdded(Details contactDetails) {
        Toast.makeText(this, "Contacts Added", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void displayRetryMessage(String errorMessage) {
        Snackbar snackbar = Snackbar
                .make(mAddContactsCoordinatorLayout, errorMessage, Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> {
                    addContactsList();
                    Snackbar snackbar1 = Snackbar.make(mAddContactsCoordinatorLayout, "Retrying !", Snackbar.LENGTH_SHORT);
                    snackbar1.show();
                });

        snackbar.show();
    }

    @Override
    public void displayErrorMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayProfileIconPickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                AddContactActivity.this);
        builder.setTitle(R.string.dialog_profile_icon_title);
        builder.setMessage(R.string.dialog_profile_icon_desc);
        builder.setNegativeButton(R.string.dialog_profile_icon_camera,
                (dialog, which) -> mAddContactPresenter.checkCameraPermissions());
        builder.setPositiveButton(R.string.dialog_profile_icon_gallery,
                (dialog, which) -> Toast.makeText(getApplicationContext(), "Gallery is clicked", Toast.LENGTH_LONG).show());
        builder.show();
    }


    @Override
    public void checkCameraPermissions() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(result == PackageManager.PERMISSION_GRANTED){
            mAddContactPresenter.cameraPermissionsGranted();
        }else {
            mAddContactPresenter.cameraPermissionsNotGranted();
       }

    }

    @Override
    public void openCameraIntent(Uri filePath) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = filePath;
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, CAMERA_INTENT);
    }

    @Override
    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSION_CODE);
    }


    @OnClick(R.id.btn_add_save)
    public void addContact() {
        addContactsList();
    }

    private void addContactsList() {
        mFName = mFirstNameTextView.getText().toString();
        mLName = mLastNameTextView.getText().toString();
        mEmail = mEmailTextView.getText().toString();
        mPhone = mPhoneTextView.getText().toString();
        mProfilePICURL = "";
        mFavorite = false;
        mCreatedAt = "";
        mUpdatedAt = "";
        mAddContactPresenter.validateData(mFName, mEmail, mPhone);

    }

    @Override
    public void addContactToList() {
        Details details = new Details(12, mFName, mLName, mEmail, mPhone, mProfilePICURL, mFavorite, mCreatedAt, mUpdatedAt);
        mAddContactPresenter.initialize(details);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                mAddContactPresenter.cameraPermissionsGranted();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_INTENT) {
            if (resultCode == RESULT_OK) {
                mProfileIcon.setImageURI(file);
            }
        }
    }

}
