package vin.com.contactmanagement.mvp.view;

import android.net.Uri;

import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.mvp.View;

/**
 * Created by Vinay
 */

public interface AddView extends View {

    void displayLoadingScreen();

    void hideLoadingScreen();

    void updateContactAdded(Details contactDetails);

    void displayRetryMessage(String errorMessage);

    void displayErrorMessage(String errorMessage);

    void displayProfileIconPickerDialog();

    void addContactToList();

    void checkCameraPermissions();

    void openCameraIntent(Uri filePath);

    void requestCameraPermission();



}
