/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package vin.com.contactmanagement.mvp.presenter;


import vin.com.contactmanagement.mvp.View;

public interface Presenter {

    void attachView(View v);
}
