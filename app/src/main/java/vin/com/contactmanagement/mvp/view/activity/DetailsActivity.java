package vin.com.contactmanagement.mvp.view.activity;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import vin.com.contactmanagement.Constants;
import vin.com.contactmanagement.R;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.mvp.DetailsView;

public class DetailsActivity extends BaseActivity implements DetailsView {

    @BindView(R.id.detailsToolbar)
    Toolbar mToolBar;

    @BindView(R.id.iv_details_profile_icon)
    CircleImageView mProfileIcon;

    @BindView(R.id.iv_details_fav_icon)
    ImageView mFavIcon;

    @BindView(R.id.tv_details_first_name)
    TextView mFirstNameTextView;

    @BindView(R.id.tv_details_last_name)
    TextView mLastNameTextView;

    @BindView(R.id.iv_details_phone_icon)
    ImageView mPhoneIcon;

    @BindView(R.id.tv_details_phone)
    TextView mPhoneTextView;

    @BindView(R.id.iv_details_email_icon)
    ImageView mEmailIcon;

    @BindView(R.id.tv_details_email)
    TextView mEmailTextView;

    ProgressDialog mProgressDialog;

    String contactID;

    boolean isFavourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        setupToolBar();
        setupProgressDialog();
        if (getIntent().getExtras() != null)
            contactID = getIntent().getExtras().getString(Constants.CONTACT_ID);
        if (contactID != null) {
            mDetailsPresenter.attachView(this);
            mDetailsPresenter.initialize(contactID);
        }

    }

    private void setupToolBar() {
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading . . . ");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }

    @Override
    public void displayLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.show();
    }

    @Override
    public void hideLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public void updateContactDetails(Details contactDetails) {
        Glide.with(this)
                .load(contactDetails.getProfilePic())
                .centerCrop()
                .placeholder(R.drawable.default_icon)
                .crossFade()
                .into(mProfileIcon);

        mFirstNameTextView.setText(contactDetails.getFirstName());
        mLastNameTextView.setText(contactDetails.getLastName());
        mPhoneTextView.setText(contactDetails.getPhoneNumber());
        mEmailTextView.setText(contactDetails.getEmail());
        if (contactDetails.getFavorite()) {
            mFavIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite_black));
            isFavourite = true;
        } else {
            mFavIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite_un_selected));
            isFavourite = false;
        }
    }

    @Override
    public void setFavouriteContact() {
        if (isFavourite) {
            isFavourite = false;
            mFavIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite_un_selected));
        } else {
            isFavourite = true;
            mFavIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite_black));
        }
    }


    @Override
    public void displayErrorMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.iv_details_phone_icon)
    public void contactIconClicked() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mPhoneTextView.getText().toString()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @OnClick(R.id.iv_details_fav_icon)
    public void updateFAVIcon() {
        String fName = mFirstNameTextView.getText().toString();
        String lName = mLastNameTextView.getText().toString();
        String email = mEmailTextView.getText().toString();
        String phone = mPhoneTextView.getText().toString();
        String profilePICURL = "";
        Boolean favorite = !isFavourite;
        String createdAt = "";
        String updatedAt = "";
        Details details = new Details(Integer.parseInt(contactID), fName, lName, email, phone, profilePICURL, favorite, createdAt, updatedAt);
        mDetailsPresenter.updateContactDetails(contactID, details);
    }

    @OnClick(R.id.tv_details_email)
    public void emailIconClicked() {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:" + mEmailTextView.getText().toString()));
        try {
            startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Email Client not Found", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.iv_details_email_icon)
    public void messageIconClicked(){
        Intent intent_sms = new Intent(Intent.ACTION_VIEW);
        intent_sms.setData(Uri.parse("sms:"));
        intent_sms.putExtra("sms_body", "Hello");
        startActivity(intent_sms);
    }

}
