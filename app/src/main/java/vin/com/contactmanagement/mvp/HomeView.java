package vin.com.contactmanagement.mvp;

import java.util.List;

import vin.com.contactmanagement.data.entity.Contacts;

/**
 * Created by Vinay
 */

public interface HomeView extends View {

    void displayLoadingScreen();

    void hideLoadingScreen();

    void updateContactsList(List<Contacts> starredList);

    void displayErrorMessage(String errorMessage);
}
