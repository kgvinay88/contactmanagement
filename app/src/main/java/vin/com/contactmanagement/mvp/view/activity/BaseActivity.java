package vin.com.contactmanagement.mvp.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.inject.Inject;

import vin.com.contactmanagement.ContactsApplication;
import vin.com.contactmanagement.di.components.ApplicationComponent;
import vin.com.contactmanagement.di.components.DaggerUseCaseComponent;
import vin.com.contactmanagement.di.modules.ActivityModule;
import vin.com.contactmanagement.di.modules.UseCaseModule;
import vin.com.contactmanagement.mvp.presenter.AddContactPresenter;
import vin.com.contactmanagement.mvp.presenter.DetailsPresenter;
import vin.com.contactmanagement.mvp.presenter.HomePresenter;

public class BaseActivity extends AppCompatActivity {

    @Inject
    HomePresenter mHomePresenter;

    @Inject
    DetailsPresenter mDetailsPresenter;

    @Inject
    AddContactPresenter mAddContactPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerUseCaseComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .useCaseModule(new UseCaseModule())
                .build().inject(this);
    }

    private ApplicationComponent getApplicationComponent() {
        return ((ContactsApplication) getApplication()).getApplicationComponent();
    }

    private ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }
}
