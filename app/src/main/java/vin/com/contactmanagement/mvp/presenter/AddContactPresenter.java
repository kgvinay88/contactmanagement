package vin.com.contactmanagement.mvp.presenter;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.Patterns;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.domain.AddContactsUseCase;
import vin.com.contactmanagement.mvp.View;
import vin.com.contactmanagement.mvp.view.AddView;

/**
 * Created by Vinay
 */

public class AddContactPresenter implements Presenter {

    private AddView mView;
    private final AddContactsUseCase mAddContactUseCase;
    private ContactObservableRepoDb mDatabase;


    @Inject
    public AddContactPresenter(AddContactsUseCase addContactsUseCase, ContactObservableRepoDb database) {
        mAddContactUseCase = addContactsUseCase;
        mDatabase = database;
    }

    @Override
    public void attachView(View v) {
        mView = (AddView) v;
    }

    public void initialize(Details contactDetails) {
        mView.displayLoadingScreen();
        mAddContactUseCase.setContactDetails(contactDetails);
        mAddContactUseCase.execute()
                .subscribe(this::onContactAdded, this::onContactsError);

    }

    private void onContactsError(Throwable throwable) {
        mView.hideLoadingScreen();
        if (throwable.toString().contains("java.net.UnknownHostException")) {
            mView.displayRetryMessage("Unable to Contact Server, Kindly Retry");
        }
        throwable.printStackTrace();
    }

    public void onContactAdded(Details details) {
        mView.hideLoadingScreen();
        mView.updateContactAdded(details);
        addContactToDatabase(details);

    }

    public void addContactToDatabase(Details details) {
        Contacts contacts = new Contacts("" + details.getId(), details.getFirstName(), details.getLastName(), details.getProfilePic());
        mDatabase.insertRepo(contacts);
    }

    public void validateData(String fName, String email, String phone) {
      if (validateName(fName) && validateEmail(email) && validatePhone(phone)) {
            mView.addContactToList();
        }
    }

    public boolean validateName(String name) {
        if (name != null && name.length() < 3) {
            mView.displayErrorMessage("First Name is not Valid");
            return false;
        }
        return true;
    }

    public boolean validatePhone(String phone) {
        if (!(android.util.Patterns.PHONE.matcher(phone).matches() && (phone.length() > 10))) {
            mView.displayErrorMessage("Phone Number is not Valid");
            return false;
        }
        return true;
    }

    public boolean validateEmail(String email) {
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.displayErrorMessage("Email Address is not Valid");
            return false;
        }
        return true;
    }

    public void checkCameraPermissions() {
        mView.checkCameraPermissions();
    }

    public void cameraPermissionsGranted() {
        mView.openCameraIntent(Uri.fromFile(getOutputMediaFile()));
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    public void cameraPermissionsNotGranted() {
        mView.requestCameraPermission();
    }
}
