package vin.com.contactmanagement.mvp.view.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vin.com.contactmanagement.Constants;
import vin.com.contactmanagement.IndexLayout;
import vin.com.contactmanagement.R;
import vin.com.contactmanagement.adapters.HomeDataAdapter;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.mvp.HomeView;

public class HomeActivity extends BaseActivity implements HomeView{

    @BindView(R.id.homeToolbar)
    Toolbar mToolBar;

    @BindView(R.id.contactsListRecyclerView)
    RecyclerView mContactsListRecyclerView;

   @BindView(R.id.addContactFAB)
   FloatingActionButton mAddContactFAB;

    ProgressDialog mProgressDialog;

    HomeDataAdapter mAdapter;

    @BindView(R.id.index_layout)
    IndexLayout mIndexFrameLayout;

    private List<Contacts> mContactsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);
        setupProgressDialog();
        setupAdapter();
        mHomePresenter.attachView(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mHomePresenter.initialize();
    }

    private void setupAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mContactsListRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new HomeDataAdapter(this, mContactsList, item -> {
            Intent mIntent = new Intent(HomeActivity.this, DetailsActivity.class);
            mIntent.putExtra(Constants.CONTACT_ID, item.getId());
            startActivity(mIntent);
        });
        mContactsListRecyclerView.setAdapter(mAdapter);
        mIndexFrameLayout.attach(mContactsListRecyclerView);
    }

    private void setupProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading . . . ");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }

    @Override
    public void displayLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.show();
    }

    @Override
    public void hideLoadingScreen() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    @Override
    public void updateContactsList(List<Contacts> starredList) {
        mContactsList = starredList;
        setupAdapter();
    }

    @Override
    public void displayErrorMessage(String errorMessage) {

    }

    @OnClick(R.id.addContactFAB)
    public void addContactClicked(){
        Intent addContactIntent = new Intent(HomeActivity.this,AddContactActivity.class);
        startActivity(addContactIntent);
    }
}
