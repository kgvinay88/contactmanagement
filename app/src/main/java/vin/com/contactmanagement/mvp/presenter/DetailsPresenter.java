package vin.com.contactmanagement.mvp.presenter;

import javax.inject.Inject;

import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.domain.GetDetailsUseCase;
import vin.com.contactmanagement.domain.UpdateContactUseCase;
import vin.com.contactmanagement.mvp.DetailsView;
import vin.com.contactmanagement.mvp.View;

/**
 * Created by Vinay
 */

public class DetailsPresenter implements Presenter {


    private DetailsView mView;
    private final GetDetailsUseCase mDetailsUseCase;
    private final UpdateContactUseCase mUpdateContactsUseCase;


    @Inject
    public DetailsPresenter(GetDetailsUseCase detailsUseCase,UpdateContactUseCase updateContactUseCase) {
        mDetailsUseCase = detailsUseCase;
        mUpdateContactsUseCase = updateContactUseCase;
    }

    @Override
    public void attachView(View v) {
        mView = (DetailsView) v;
    }

    public void initialize(String contactID) {
        mView.displayLoadingScreen();
        mDetailsUseCase.setContactID(contactID);
        mDetailsUseCase.execute()
                .subscribe(this::onDetailsReceived, this::onDetailsError);

    }

    private void onDetailsError(Throwable throwable) {
        mView.hideLoadingScreen();
        mView.displayErrorMessage("Unable to load data");
        throwable.printStackTrace();
    }

    private void onDetailsReceived(Details details) {
        mView.hideLoadingScreen();
        mView.updateContactDetails(details);
    }

    public void updateContactDetails(String contactID, Details contactDetails){
        mView.displayLoadingScreen();
        mUpdateContactsUseCase.setContactDetails(contactID,contactDetails);
        mUpdateContactsUseCase.execute()
                .subscribe(this::onContactUpdated,this::onContactUpdateFailed);

    }

    private void onContactUpdateFailed(Throwable throwable) {
        mView.hideLoadingScreen();
        throwable.printStackTrace();
    }

    private void onContactUpdated(Details details) {
        mView.hideLoadingScreen();
        mView.setFavouriteContact();
    }
}
