package vin.com.contactmanagement.data.repository;

import java.util.List;

import rx.Observable;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;

/**
 * Created by Vinay
 */

public interface ContactsRepository {

    Observable<List<Contacts>> getContacts();

    Observable<Details> getDetails(String userID);

    Observable<Details> addContact(Details contactDetails);

    Observable<Details> updateContact(String userID,Details contactDetails);
}
