package vin.com.contactmanagement.data.net;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay
 */

public class RestAPIImpl implements ContactsRepository {

    RestApi mRestAPI;
    private static final long RETRY_COUNT = 5;

    @Inject
    public RestAPIImpl(RestApi restAPI) {
        this.mRestAPI = restAPI;
    }



    @Override
    public Observable<List<Contacts>> getContacts() {
        return mRestAPI.getContacts().retry(RETRY_COUNT);
    }

    @Override
    public Observable<Details> getDetails(String userID) {
        return mRestAPI.getDetails(userID).retry(RETRY_COUNT);
    }

    @Override
    public Observable<Details> addContact(Details contactDetails) {
        return mRestAPI.addContact(contactDetails).retry(RETRY_COUNT);
    }

    @Override
    public Observable<Details> updateContact(String userID,Details contactDetails) {
        return mRestAPI.updateContact(userID,contactDetails).retry(RETRY_COUNT);
    }
}
