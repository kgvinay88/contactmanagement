package vin.com.contactmanagement.data.net;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;

/**
 * Created by Vinay .
 */

public interface RestApi {

    @GET("contacts.json")
    Observable<List<Contacts>> getContacts();

    @GET("contacts/{id}.json")
    Observable<Details> getDetails(@Path("id") String userID);

    @POST("contacts.json")
    Observable<Details> addContact(@Body Details contactDetails);

    @PUT("contacts/{id}.json")
    Observable<Details> updateContact(@Path("id") String userID,@Body Details contactDetails);
}
