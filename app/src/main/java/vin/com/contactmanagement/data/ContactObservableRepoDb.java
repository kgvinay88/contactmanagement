/*
 * Copyright (C) 2015 Federico Paolinelli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package vin.com.contactmanagement.data;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;
import vin.com.contactmanagement.data.entity.Contacts;

public class ContactObservableRepoDb {

    private PublishSubject<List<Contacts>> mSubject = PublishSubject.create();
    private ContactRepoDbHelper mDbHelper;

    public ContactObservableRepoDb(Context c) {
        mDbHelper = new ContactRepoDbHelper(c);
    }

    public Observable<List<Contacts>> getObservable() {
        Observable<List<Contacts>> firstTimeObservable =
                Observable.fromCallable(this::getAllReposFromDb);
        return firstTimeObservable.concatWith(mSubject);
    }

    private List<Contacts> getAllReposFromDb() {
        mDbHelper.openForRead();
        List<Contacts> repos = new ArrayList<>();
        Cursor c = mDbHelper.getAllRepo();
        if (!c.moveToFirst()) {
            return repos;
        }
        do {
            repos.add(new Contacts(c.getString(ContactRepoDbHelper.REPO_ID_COLUMN_POSITION),
                               c.getString(ContactRepoDbHelper.REPO_FIRSTNAME_COLUMN_POSITION),
                               c.getString(ContactRepoDbHelper.REPO_LASTNAME_COLUMN_POSITION),
                              c.getString(ContactRepoDbHelper.REPO_PROFILE_PIC_URL_POSITION)));
        } while (c.moveToNext());
        c.close();
        mDbHelper.close();
        return repos;
    }

    public void insertRepoList(List<Contacts> contactsList) {
        mDbHelper.open();
        mDbHelper.removeAllRepo();
        for (Contacts contact : contactsList) {
            mDbHelper.addRepo(contact.getId(),
                    contact.getFirstName(),
                    contact.getLastName(),
                    contact.getProfilePic());
        }
        mDbHelper.close();
        mSubject.onNext(contactsList);
    }

    public void insertRepo(Contacts contact) {
        mDbHelper.open();
        mDbHelper.addRepo(contact.getId(),
                contact.getFirstName(),
                contact.getLastName(),
                contact.getProfilePic());

        mDbHelper.close();

        List<Contacts> result = getAllReposFromDb();
        mSubject.onNext(result);
    }
}
