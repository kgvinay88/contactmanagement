
package vin.com.contactmanagement.di.modules;

import android.app.Activity;
import dagger.Module;
import dagger.Provides;
import vin.com.contactmanagement.di.PerActivity;

@Module
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity providesActivity() {
        return this.activity;
    }


}
