package vin.com.contactmanagement.di.modules;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import vin.com.contactmanagement.ContactsApplication;
import vin.com.contactmanagement.Util;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.net.RestAPIImpl;
import vin.com.contactmanagement.data.net.RestApi;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay
 */
@Module
public class ApplicationModule {
    private final ContactsApplication mApplication;
    String mBaseUrl = "http://gojek-contacts-app.herokuapp.com/";

    public ApplicationModule(ContactsApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    ContactsApplication provideAndroidApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }



    @Provides
    @Named("executor_thread")
    Scheduler provideExecutorThread() {
        return Schedulers.newThread();
    }

    @Provides
    @Named("ui_thread")
    Scheduler provideUiThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    Cache provideHttpCache(ContactsApplication application) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);
    }

    @Provides
    String provideBaseURL() {
        return mBaseUrl;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache, Context context) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(chain -> {
            Request request = chain.request();
            if (!Util.isInternetConnection(context)) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();
                request = request.newBuilder()
                        .header("Accept", "application/json;odata=verbose")
                        .cacheControl(cacheControl)
                        .method(request.method(), request.body())
                        .build();
            } else {
                request = request.newBuilder()
                        .header("Accept", "application/json;odata=verbose")
                        .method(request.method(), request.body())
                        .build();
            }
            Log.d("API Call", "URL : " + request.url());
            return chain.proceed(request);
        });

        client.cache(cache);
        return client.build();
    }

    @Provides
    @Singleton
    RestApi provideRestApi(Gson gson, OkHttpClient okHttpClient, String baseURL) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseURL)
                .client(okHttpClient)
                .build()
                .create(RestApi.class);
    }

    @Provides
    @Singleton
    ContactsRepository provideDataRepository(RestAPIImpl restDataSource) {
        return restDataSource;
    }


    @Provides
    @Singleton
    ContactObservableRepoDb provideContactsRepoDbObservable() {
        return new ContactObservableRepoDb(mApplication.getApplicationContext());
    }



}
