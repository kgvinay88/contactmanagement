package vin.com.contactmanagement.di.components;

/**
 * Created by Vinay
 */


import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;
import vin.com.contactmanagement.ContactsApplication;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.repository.ContactsRepository;
import vin.com.contactmanagement.di.modules.ApplicationModule;
import vin.com.contactmanagement.mvp.view.activity.BaseActivity;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);

    ContactsApplication application();

    Context applicationContext();

    ContactsRepository contactsRepository();

    ContactObservableRepoDb contactsRepoDB();

    @Named("ui_thread")
    Scheduler uiThread();

    @Named("executor_thread")
    Scheduler executorThread();
}
