package vin.com.contactmanagement.di.modules;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import vin.com.contactmanagement.ContactsApplication;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.repository.ContactsRepository;
import vin.com.contactmanagement.di.PerActivity;
import vin.com.contactmanagement.domain.AddContactsUseCase;
import vin.com.contactmanagement.domain.GetContactsDataUseCase;
import vin.com.contactmanagement.domain.GetDetailsUseCase;
import vin.com.contactmanagement.domain.UpdateContactUseCase;

/**
 * Created by Vinay
 */
@Module
public class UseCaseModule {

    public UseCaseModule() {

    }

    @Provides
    @PerActivity
    GetContactsDataUseCase provideContactListUseCase(ContactsRepository contactsRepository, @Named("ui_thread") Scheduler uiThread,
                                                     @Named("executor_thread") Scheduler executorThread,ContactObservableRepoDb database) {
        return new GetContactsDataUseCase(contactsRepository, uiThread, executorThread,database);
    }


    @Provides
    @PerActivity
    GetDetailsUseCase provideDetailsUseCase(ContactsRepository contactsRepository, @Named("ui_thread") Scheduler uiThread,
                                                @Named("executor_thread") Scheduler executorThread) {
        return new GetDetailsUseCase(contactsRepository, uiThread, executorThread);
    }


    @Provides
    @PerActivity
    AddContactsUseCase provideAddContactsUseCase(ContactsRepository contactsRepository, @Named("ui_thread") Scheduler uiThread,
                                                 @Named("executor_thread") Scheduler executorThread) {
        return new AddContactsUseCase(contactsRepository, uiThread, executorThread);
    }

    @Provides
    @PerActivity
    UpdateContactUseCase provideUpdateContactsUseCase(ContactsRepository contactsRepository, @Named("ui_thread") Scheduler uiThread,
                                                   @Named("executor_thread") Scheduler executorThread) {
        return new UpdateContactUseCase(contactsRepository, uiThread, executorThread);
    }


}
