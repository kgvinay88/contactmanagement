package vin.com.contactmanagement.di.components;

import dagger.Component;
import vin.com.contactmanagement.di.PerActivity;
import vin.com.contactmanagement.di.modules.ActivityModule;
import vin.com.contactmanagement.di.modules.ApplicationModule;
import vin.com.contactmanagement.di.modules.UseCaseModule;
import vin.com.contactmanagement.domain.GetContactsDataUseCase;
import vin.com.contactmanagement.mvp.view.activity.BaseActivity;

/**
 * Created by Vinay
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {UseCaseModule.class, ActivityModule.class})
public interface UseCaseComponent extends ActivityComponent{
    void inject(BaseActivity baseActivity);

}
