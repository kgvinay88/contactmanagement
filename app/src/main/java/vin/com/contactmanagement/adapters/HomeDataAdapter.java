package vin.com.contactmanagement.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import vin.com.contactmanagement.R;
import vin.com.contactmanagement.data.entity.Contacts;

/**
 * Created by Vinay .
 */

public class HomeDataAdapter extends RecyclerView.Adapter<HomeDataAdapter.HomeDataViewHolder> {

    private List<Contacts> mDataList;
    private LinkedHashMap<String, Integer> mMapIndex;
    private ArrayList<String> mSectionList;
    private String[] mSections;
    private Context mContext;
    private final OnItemClickListener listener;

    public HomeDataAdapter(Context context,List<Contacts> dataList,OnItemClickListener listener) {
        mContext = context;
        mDataList = dataList;
        this.listener = listener;

        fillSections();
    }

    private void fillSections() {
        mMapIndex = new LinkedHashMap<>();
        for (int x = 0; x < mDataList.size(); x++) {
            String firstName = mDataList.get(x).getFirstName();
            if (firstName.length() > 1) {
                String ch = firstName.substring(0, 1);
                ch = ch.toUpperCase();
                if (!mMapIndex.containsKey(ch)) {
                    mMapIndex.put(ch, x);
                }
            }
        }
        Set<String> sectionLetters = mMapIndex.keySet();
        mSectionList = new ArrayList<>(sectionLetters);
        Collections.sort(mSectionList);

        mSections = new String[mSectionList.size()];
        mSectionList.toArray(mSections);
    }

    private String getSection(Contacts pContact) {
        return pContact.getFirstName().substring(0, 1).toUpperCase();
    }

    @Override
    public HomeDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);
        return new HomeDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeDataViewHolder holder, int position) {
        Contacts contact = mDataList.get(position);
        String section = getSection(contact);
        holder.bind(contact, section, mMapIndex.get(section) == position);
    }

    public interface OnItemClickListener {
        void onItemClick(Contacts item);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class HomeDataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_contactName_first)
        TextView mContactFirstName;
        @BindView(R.id.tv_contactName_last)
        TextView mContactLastName;
        @BindView(R.id.section_title)
        TextView mSectionTitle;
        @BindView(R.id.iv_contactIcon)
        de.hdodenhof.circleimageview.CircleImageView mContactsIcon;

        public HomeDataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Contacts contactItem, String contactSection, boolean isShown) {
            mContactFirstName.setText(contactItem.getFirstName());
            mContactLastName.setText(contactItem.getLastName());
            Glide.with(mContext)
                    .load(contactItem.getProfilePic())
                    .centerCrop()
                    .placeholder(R.drawable.default_icon)
                    .crossFade()
                    .into(mContactsIcon);
            mSectionTitle.setText(contactSection);
            mSectionTitle.setVisibility(isShown ? View.VISIBLE : View.GONE);

            itemView.setOnClickListener(v -> listener.onItemClick(contactItem));
        }


    }
}
