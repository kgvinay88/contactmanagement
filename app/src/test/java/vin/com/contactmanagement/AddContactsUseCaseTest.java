package vin.com.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;
import rx.Scheduler;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.data.repository.ContactsRepository;
import vin.com.contactmanagement.domain.AddContactsUseCase;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.only;

/**
 * Created by Vinay
 */

public class AddContactsUseCaseTest {

    @Mock
    ContactsRepository mRepository;

    @Mock
    Scheduler mockUiScheduler;

    @Mock
    Scheduler mockExecutorScheduler;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test public void testThatAddContactsUsecaseImplementsAnUsecase() throws Exception{
        AddContactsUseCase charactersUsecase = givenAAddContactsUseCase();

        assertThat(charactersUsecase, instanceOf(AddContactsUseCase.class));
    }

    @Test
    public void testThatDetailUsecaseIsCalledOnce() throws Exception {
        AddContactsUseCase contactsUseCase = givenAAddContactsUseCase();

        when(mRepository.addContact(givenAContactDetails())).thenReturn(getFakeContactObservable());
        contactsUseCase.execute();

        Mockito.verify(mRepository, only()).addContact(givenAContactDetails());
    }

    private Details givenAContactDetails() {
      return new Details(1, "testFirstName", "testLastName", "test@gmail.com", "1234567890", "", Boolean.FALSE, "", "");
    }

    private AddContactsUseCase givenAAddContactsUseCase() {
        return new AddContactsUseCase(mRepository, null, null);
    }

    private Observable<Details> getFakeContactObservable() {
        return Observable.just(givenAContactDetails());
    }
}
