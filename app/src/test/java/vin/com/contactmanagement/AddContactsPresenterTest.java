package vin.com.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.domain.AddContactsUseCase;
import vin.com.contactmanagement.mvp.presenter.AddContactPresenter;
import vin.com.contactmanagement.mvp.view.AddView;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Vinay
 */

public class AddContactsPresenterTest {

    @Mock
    AddContactsUseCase mAddContactUseCase;

    @Mock
    ContactObservableRepoDb mDatabase;

    @Mock
    AddView mView;

    @Mock
    AddContactPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isContactBeingAddedToDatabase() throws Exception {
        AddContactPresenter addContactPresenter = getMockPresenter();

        Details details = getMockDetailsData();
        Contacts contacts = new Contacts(""+details.getId(),details.getFirstName(),details.getLastName(),details.getProfilePic());
        addContactPresenter.onContactAdded(details);
        verify(mDatabase, times(1))
                .insertRepo(contacts);
    }

    @Test
    public void isNameBeenValidated(){
        AddContactPresenter addContactPresenter = getMockPresenter();

        when(mPresenter.validateName("vin")).thenReturn(true);

        addContactPresenter.validateData("","","");

        verify(mView,times(1))
                .displayErrorMessage("First Name is not Valid");

      }

    @Test
    public void isEmailBeenValidated(){
        AddContactPresenter addContactPresenter = getMockPresenter();

        when(mPresenter.validateEmail("vin")).thenReturn(false);

        addContactPresenter.validateData("","","");

        verify(mView,times(1))
                .displayErrorMessage("Email Address is not Valid");

    }

    private Details getMockDetailsData() {
        return new Details(123, "testFirstName", "testLastName", "test@gmail.com", "1234567890", "", false, "", "");
    }

    private AddContactPresenter getMockPresenter() {
        AddContactPresenter listPresenter = new AddContactPresenter(mAddContactUseCase, mDatabase);
        listPresenter.attachView(mView);
        return listPresenter;
    }
}
