package vin.com.contactmanagement;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import vin.com.contactmanagement.domain.GetDetailsUseCase;
import vin.com.contactmanagement.domain.UpdateContactUseCase;
import vin.com.contactmanagement.mvp.DetailsView;


/**
 * Created by Vinay
 */

public class DetailsPresenterTest {

    @Mock
    GetDetailsUseCase mDetailsUseCase;

    @Mock
    UpdateContactUseCase mUpdateContactsUseCase;

    @Mock
    DetailsView mView;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
}
