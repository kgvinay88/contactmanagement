package vin.com.contactmanagement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.domain.GetContactsDataUseCase;
import vin.com.contactmanagement.mvp.HomeView;
import vin.com.contactmanagement.mvp.presenter.HomePresenter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Vinay
 */

public class HomePresenterTest {

    @Mock
    GetContactsDataUseCase mContactsUseCase;

    @Mock
    HomeView mockHomeView;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testThatContactsArePassedToTheView() throws Exception {
        HomePresenter homePresenter = getMockHomePresenter();

        List<Contacts> contactList = getMockContactsStarredList();
        homePresenter.onContactsReceived(contactList);
        verify(mockHomeView, times(1))
                .updateContactsList(contactList);
    }

    @Test
    public void testThatErrorMessageBeingDisplayed() throws Exception {
        HomePresenter homePresenter = getMockHomePresenter();

        when(mContactsUseCase.execute()).thenReturn(Observable.error(new Exception()));
        homePresenter.initialize();
        verify(mockHomeView, times(1))
                .displayErrorMessage("Unable to load data");

    }

     private List<Contacts> getMockContactsStarredList() {
        List<Contacts> contactsList = new ArrayList<>();

        Contacts contact1 = new Contacts("1", "firstName_1", "lastName_1", "profilePic_1");
        Contacts contact2 = new Contacts("2", "firstName_2", "lastName_2", "profilePic_2");
        Contacts contact3 = new Contacts("3", "firstName_3", "lastName_3", "profilePic_3");
        contactsList.add(contact1);
        contactsList.add(contact2);
        contactsList.add(contact3);
        return contactsList;
    }

    private HomePresenter getMockHomePresenter() {
        HomePresenter listPresenter = new HomePresenter(mContactsUseCase);
        listPresenter.attachView(mockHomeView);
        return listPresenter;
    }
}
