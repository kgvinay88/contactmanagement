package vin.com.contactmanagement.data;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.http.Body;
import retrofit2.http.Path;
import rx.Observable;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.entity.Details;
import vin.com.contactmanagement.data.net.RestApi;

/**
 * Created by Vinay .
 */

public class MockAPIClient implements RestApi {


    @Override
    public Observable<List<Contacts>> getContacts() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Contacts>>() {}.getType();
        List<Contacts> contactsList =  gson.fromJson(TestContactsData.TEST_CONTACTS_DATA, type);
        return Observable.just(contactsList).delay(1, TimeUnit.SECONDS);
    }

    @Override
    public Observable<Details> getDetails(@Path("id") String userID) {
        return null;
    }

    @Override
    public Observable<Details> addContact(@Body Details contactDetails) {
        return null;
    }

    @Override
    public Observable<Details> updateContact(@Path("id") String userID, @Body Details contactDetails) {
        return null;
    }


}
