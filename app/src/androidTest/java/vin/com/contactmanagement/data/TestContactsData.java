package vin.com.contactmanagement.data;

/**
 * Created by Vinay .
 */

public class TestContactsData {
    public static final String TEST_CONTACTS_DATA = "[\n" +
            "  {\n" +
            "    \"id\": 64,\n" +
            "    \"first_name\": \"Amitabh\",\n" +
            "    \"last_name\": \"Bachchan\",\n" +
            "    \"profile_pic\": \"/images/missing.png\",\n" +
            "    \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/64.json\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 65,\n" +
            "    \"first_name\": \"Winhi\",\n" +
            "    \"last_name\": \"Bachchan\",\n" +
            "    \"profile_pic\": \"/images/missing.png\",\n" +
            "    \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/65.json\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 15,\n" +
            "    \"first_name\": \"rahul\",\n" +
            "    \"last_name\": \"panchal\",\n" +
            "    \"profile_pic\": \"/images/missing.png\",\n" +
            "    \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/15.json\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 16,\n" +
            "    \"first_name\": \"Prateek\",\n" +
            "    \"last_name\": \"gojek\",\n" +
            "    \"profile_pic\": \"/images/missing.png\",\n" +
            "    \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/16.json\"\n" +
            "  }\n" +
            "]";
}
