package vin.com.contactmanagement;


import vin.com.contactmanagement.di.DaggerTestAppComponent;
import vin.com.contactmanagement.di.TestAppComponent;
import vin.com.contactmanagement.di.TestAppModule;

/**
 * Created by Vinay .
 */

public class TestApplication extends ContactsApplication {

    private TestAppComponent testAppComponent;

    @Override
    public void onCreate() {
        initializeInjector();
        super.onCreate();
    }

    private void initializeInjector() {
       this.testAppComponent = DaggerTestAppComponent.builder()
                .testAppModule(new TestAppModule(this))
                .build();
    }

    public TestAppComponent getApplicationComponent() {
        return this.testAppComponent;
    }
}
