package vin.com.contactmanagement.tests;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import javax.inject.Inject;

import vin.com.contactmanagement.R;
import vin.com.contactmanagement.TestApplication;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.entity.Contacts;
import vin.com.contactmanagement.data.net.RestApi;
import vin.com.contactmanagement.mvp.view.activity.HomeActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Vinay .
 */

@RunWith(AndroidJUnit4.class)
public class HomeActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> homeActivityTestRule =
            new ActivityTestRule<>(HomeActivity.class,true,true);

    @Inject
    RestApi mockRestAPI;


    @Before
    public void setUp() {
        ((TestApplication) homeActivityTestRule.getActivity().getApplication()).getApplicationComponent().inject(this);
    }

    @Test
    public void toolbarTitle() {
        CharSequence title = InstrumentationRegistry.getTargetContext()
                .getString(R.string.title_activity_home);
        matchToolbarTitle(title);
    }

    private static ViewInteraction matchToolbarTitle(
            CharSequence title) {
        return onView(
                allOf(isAssignableFrom(TextView.class),
                        withParent(isAssignableFrom(Toolbar.class))))
                .check(matches(withText(title.toString())));
    }

    @Test
    public void isContactsLoadedCorrectly(){
        onView(withId(R.id.contactsListRecyclerView))
                .check(matches(hasDescendant(withText("Amitabh"))));
    }

    @Test
    public void isAdapterDisplayingFirstName() {
        List<Contacts> contactsList = mockRestAPI.getContacts().toBlocking().first();

        onView(withId(R.id.contactsListRecyclerView))
                .check(matches(hasDescendant(withText(contactsList.get(0).getFirstName()))));

    }


}
