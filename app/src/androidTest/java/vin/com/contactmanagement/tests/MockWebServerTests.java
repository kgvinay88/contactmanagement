package vin.com.contactmanagement.tests;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import vin.com.contactmanagement.R;
import vin.com.contactmanagement.RestServiceTestHelper;
import vin.com.contactmanagement.mvp.view.activity.HomeActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Created by Vinay
 */

@RunWith(AndroidJUnit4.class)
public class MockWebServerTests  extends InstrumentationTestCase {

    @Rule
    public ActivityTestRule<HomeActivity> homeActivityTestRule =
            new ActivityTestRule<>(HomeActivity.class,true,true);

    private MockWebServer server;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        server = new MockWebServer();
        server.start();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());

    }

    @Test
    public void testCorrectContactListIsDisplayed() throws Exception {
        String fileName = "contacts_ok_response.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(RestServiceTestHelper.getStringFromFile(getInstrumentation().getContext(), fileName)));

        Intent intent = new Intent();
        homeActivityTestRule.launchActivity(intent);

        onView(withId(R.id.contactsListRecyclerView))
                .check(matches(hasDescendant(withText("rahul"))));

    }
}
