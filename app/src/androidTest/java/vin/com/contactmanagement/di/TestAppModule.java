package vin.com.contactmanagement.di;

import android.content.Context;


import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.mock.BehaviorDelegate;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import vin.com.contactmanagement.TestApplication;
import vin.com.contactmanagement.data.ContactObservableRepoDb;
import vin.com.contactmanagement.data.MockAPIClient;
import vin.com.contactmanagement.data.net.RestAPIImpl;
import vin.com.contactmanagement.data.net.RestApi;
import vin.com.contactmanagement.data.repository.ContactsRepository;

/**
 * Created by Vinay .
 */
@Module
public class TestAppModule {


    private final TestApplication mApplication;


    public TestAppModule(TestApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    TestApplication provideAndroidApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
     Context provideAppContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    RestApi provideRESTApiClient() {
        return new MockAPIClient();
    }

    @Provides
    @Named("executor_thread")
    Scheduler provideExecutorThread() {
        return Schedulers.newThread();
    }

    @Provides
    @Named("ui_thread")
    Scheduler provideUiThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    ContactsRepository provideDataRepository(RestAPIImpl restDataSource) {
        return restDataSource;
    }

    @Provides
    @Singleton
    ContactObservableRepoDb provideContactsRepoDbObservable() {
        return new ContactObservableRepoDb(mApplication.getApplicationContext());
    }

}
